'use strict';
const Indexer = require('./indexer');
const MatterCloud = require('./mattercloud');
const logger = console;

class Agent {
  constructor(config = {}) {
    this.config = config;
    this.api = new MatterCloud(config.MATTERCLOUD_KEY, config.logger || logger);
    this.indexer = new Indexer(config.DB, 'mattercloud', config.NETWORK, config.FETCH_LIMIT, 4, logger,
      config.START_HEIGHT, config.MEMPOOL_EXPIRATION);
  }

  start() {
    return this.indexer.start();
  }

  stop() {
    return this.indexer.stop();
  }

}

module.exports = Agent;
