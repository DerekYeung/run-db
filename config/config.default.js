/* eslint valid-jsdoc: "off" */

'use strict';
require('dotenv').config();

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1615100747010_7976';

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };
  userConfig.customLoader = {
    utils: {
      directory: 'app/utils',
    },
  };

  const MATTERCLOUD_KEY = process.env.MATTERCLOUD_KEY;
  const NETWORK = process.env.NETWORK || 'main';
  const DB = process.env.DB || appInfo.root + '/run.db';
  const WORKERS = process.env.WORKERS || 4;
  const FETCH_LIMIT = process.env.FETCH_LIMIT || 20;
  const START_HEIGHT = process.env.START_HEIGHT || (NETWORK === 'test' ? 1382000 : 650000);
  const TIMEOUT = process.env.TIMEOUT || 10000;
  const MEMPOOL_EXPIRATION = process.env.MEMPOOL_EXPIRATION || 60 * 60 * 24;

  userConfig.rundb = {
    MATTERCLOUD_KEY,
    NETWORK,
    DB,
    WORKERS,
    FETCH_LIMIT,
    START_HEIGHT,
    TIMEOUT,
    MEMPOOL_EXPIRATION,
  };

  return {
    ...config,
    ...userConfig,
  };
};
