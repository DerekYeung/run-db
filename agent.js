'use strict';
const Agent = require('./lib/agent');
module.exports = agent => {
  const worker = new Agent(agent.config.rundb);
  worker.start();
};
